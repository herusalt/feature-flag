package main

import (
	"log"

	unleash "github.com/Unleash/unleash-client-go/v3"
)

type metricsInterface struct {
}

func init() {

	unleash.Initialize(
		unleash.WithUrl("https://gitlab.com/api/v4/feature_flags/unleash/45203187"),
		unleash.WithInstanceId("Yf5JigsVfYVs5GhsVRV_"),
		unleash.WithAppName("production"),
		unleash.WithListener(&metricsInterface{}),
	)

}

func main() {
	if unleash.IsEnabled("test_go") {
		log.Println("Feature enabled")
	} else {
		log.Println("Feature disabled")
	}
}
