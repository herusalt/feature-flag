<?php
require 'vendor/autoload.php';



use Unleash\Client\UnleashBuilder;

$unleash = UnleashBuilder::create()
    ->withAppName('production')
    ->withAppUrl('https://gitlab.com/api/v4/feature_flags/unleash/45203187')
    ->withInstanceId('Yf5JigsVfYVs5GhsVRV_')
    ->build();

if ($unleash->isEnabled('test_php')) {
    echo "Feature Enable".PHP_EOL;
}else{
    echo "Feature Disable". PHP_EOL;
}
